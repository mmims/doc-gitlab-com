# Introduction

[Doc-gitlab-com](https://gitlab.com/gitlab-com/doc-gitlab-com/) generates html pages for the [GitLab documentation website](http://doc.gitlab.com/). The documentation is generated from the markdown files in /doc directory of the GitLab repo's. The conversion from markdown into html is done with [Pandoc](http://johnmacfarlane.net/pandoc/).

# Testing

```bash
ssh root@doc.gitlab.com
cd /root/doc-gitlab-com && git pull && ./generate.rb no-clone && service nginx restart
```

# Restrictions

+ All files should be in a doc/something directory, not any deeper.
+ Only png image files are supported.

# Installation

Login as **root** and execute the following commands:

```bash
apt-get update
apt-get upgrade
apt-get remove pandoc # Pandoc 1.9.1 doesn't support pipe tables.
apt-get install haskell-platform
cabal update
cabal install pandoc --ghc-options="-O0"
ln -s /root/.cabal/bin/pandoc /usr/bin/pandoc
cd /root
git clone https://gitlab.com/gitlab-com/doc-gitlab-com.git
ln -s /root/doc-gitlab-com/nginx /etc/nginx/sites-available/doc-gitlab-com
ln -s /etc/nginx/sites-available/doc-gitlab-com /etc/nginx/sites-enabled/doc-gitlab-com
rm /etc/nginx/sites-enabled/default
service nginx start
crontab -e
20 */2 * * * /root/doc-gitlab-com/generate.rb
```

Do not forget to add the public ssh key of the server `ssh-keygen` as a deploy key to any non-public repositories in generate.rb.
